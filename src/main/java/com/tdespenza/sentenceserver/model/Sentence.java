package com.tdespenza.sentenceserver.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
import java.util.TreeMap;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
@Data
@AllArgsConstructor
public class Sentence {
    private final Map<Word.Role, String> words = new TreeMap<>();

    public void add(final Word word) {
        words.put(word.getRole(), word.getValue());
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        for (Word.Role role :
                words.keySet()) {
            builder.append(" ").append(words.get(role));
        }

        return builder.toString();
    }
}
