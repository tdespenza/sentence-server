package com.tdespenza.sentenceserver.client;

import com.tdespenza.sentenceserver.model.Word;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * FOR THE COPYCLAIM-COPYRIGHT OF THIS DOCUMENT IS BY THE MOBILE-CAPPTIVATE, LIMIT-LIABILITY-COMPANY.
 * FOR THE CREATION OF THIS DOCUMENT IS BY THE Tyshawn-LeMarcus: Despenza ON THE ~1/16/17.
 */
public interface PartOfSpeech {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    Word getWord();
}
